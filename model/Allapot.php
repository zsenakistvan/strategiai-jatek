<?php

enum Allapot{
    case MEGY;
    case ALL;
    case EPIT;
    case HARCOL;
    case KAPAL;
    case ETELTSZEREZ;
    case NYERANYAGOTSZEREZ;
    case EPUL;
    case FELEPULT;
    case ROMBOLODIK;
    case LEROMBOLT;
    case HALASZIK;
    case USZIK;
}

?>