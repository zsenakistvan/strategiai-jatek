<?php

class Ember{
    private static int $sebzes = 5;
    private int $id;
    private int $eletero;

    private function __construct(){

    }

    public static function getAllEmber(){
        try{
            $conn = DBconn::getInstance()->getConnection();
            $sql = 'CALL getAllEmber()';
            $result = mysqli_query($conn, $sql);
            $emberek = array();
            while($row = mysqli_fetch_assoc($result)){
                $ember = new Ember();
                $ember->id = $row['id'];
                $ember->eletero = $row['eletero'];
                $emberek[] = $ember;
            }
            mysqli_close($conn);
            return $emberek;
        }
        catch(Exception $ex){
            print $ex->getMessage();
            return false;
        }
    }

}


?>